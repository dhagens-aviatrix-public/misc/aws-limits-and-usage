#!/bin/bash
#Written by Dennis Hagens, dennis@aviatrix.com. This script can be modified and distributed freely. The author takes no responsibility for any impact caused by this script.
#Find the latest version on https://gitlab.com/dhagens-aviatrix-public/misc/aws-limits-and-usage

red='\033[0;31m'
green='\033[0;32m'
purple='\e[35m'
std='\033[0m'
other=''

#Check AWS CLI is installed
if ! which aws > /dev/null
  then
    echo "Make sure aws cli is installed."
    exit 1
fi

#Check if an argument is provided
if [[ $# -eq 0 ]]
  then
    echo "No Arguments found. Expected input arguments. Use -h for more information"
    exit 1
fi

if [[ $1 -eq "-h" ]]
  then
    echo "Usage: $0 <region> [<aws cli profile>]"
    exit 0
fi

#Fetch current region list                                                                                               
regionlist=`aws ec2 describe-regions --query 'Regions[*].{RegionName:RegionName}' --output text`

#Check argument is a valid region
region=$1
if ! [[ $regionlist =~ (^|[[:space:]])$region($|[[:space:]]) ]]
  then
    echo "Region is not valid. Try one of these:"
    echo "$regionlist"
    exit 1
fi

if [[ $2 ]]
  then
    profile="--profile $2"
  else
    profile=""
fi

#Start queries
echo ""
echo "Gathering information for region $region"
echo "======================================================================"
echo ""

#Instance availability
echo "======================Instance availability==========================="
regioninstancetypes=`aws ec2 describe-instance-types $profile --query 'InstanceTypes[*].{InstanceType:InstanceType}' --output text --region $region`
regionavailabilityzones=`aws ec2 describe-availability-zones $profile --query 'AvailabilityZones[*].{ZoneName:ZoneName}' --output text --region $region`

for zone in $regionavailabilityzones
  do
    missing=()
    azinstancetypes=`aws ec2 describe-instance-type-offerings $profile --location-type availability-zone --filters Name=location,Values=$zone --region $region --query InstanceTypeOfferings[*].{InstanceType:InstanceType} --output text`
    for instancetype in $regioninstancetypes
      do
        if ! [[ $azinstancetypes =~ (^|[[:space:]])$instancetype($|[[:space:]]) ]]
          then
            missing+=($instancetype)
        fi
      done
    if ! [[ -z "$missing" ]]
      then
        echo -e "${red}These instance types are available in the region $region, but not in availability zone $zone${std}"
        other=''
        for type in "${missing[@]}"
          do
            #This only prints missing instance types starting with "t", "m" and "c"
            if [[ $type =~ (^|[[:space:]])t.* ]] || [[ $type =~ (^|[[:space:]])c.* ]] || [[ $type =~ (^|[[:space:]])m.* ]]
              then
                echo -ne "$type "
              else
                #echo -ne "$type " #Uncomment to print other missing instance types
                other=true
            fi              
          done
        echo ""
        if $other
          then
            echo "There were other missing instance types that were omitted because they are not of type C, M or T"
        fi
      else
        echo -e "${green}All instance types that are available in region $region, are available in availability zone $zone${std}"
    fi
  done

echo "=====================/Instance availability==========================="
echo ""
echo "=====================Current User Privileges=========================="
user=`aws opsworks describe-my-user-profile --query 'UserProfile.Name' --region $region $profile | tr -d '"'`
echo "Username: $user"
aws iam list-attached-user-policies --user-name $user --output text --region $region $profile
echo "====================/Current User Privileges=========================="
echo ""
echo "==========================EC2 max values=============================="

usedec2=`aws ec2 describe-instances $profile --query 'Reservations[*].Instances[*].{InstanceId:InstanceId}' --output text --region $region| wc -l`
usedigw=`aws ec2 describe-internet-gateways $profile --query 'InternetGateways[*].{InternetGatewayId:InternetGatewayId}' --output text --region $region | wc -l`
usedvpc=`aws ec2 describe-vpcs $profile --query 'Vpcs[*].{VpcId:VpcId}' --output text --region $region| wc -l`
usedeip=`aws ec2 describe-addresses $profile --query 'Addresses[*].{PublicIp:PublicIp}' --output text --region $region | wc -l`

maxec2=`aws service-quotas list-service-quotas --service-code ec2 $profile --query 'Quotas[?QuotaName==\`Running On-Demand Standard (A, C, D, H, I, M, R, T, Z) instances\`]'.Value --output text --region $region`
maxigw=`aws service-quotas list-service-quotas --service-code vpc $profile --query 'Quotas[?QuotaName==\`Internet gateways per Region\`].{Value:Value}' --output text --region $region`
maxvpc=`aws service-quotas list-service-quotas --service-code vpc $profile --query 'Quotas[?QuotaName==\`VPCs per Region\`].{Value:Value}' --output text --region $region`
maxeip=`aws service-quotas list-service-quotas --service-code ec2 $profile --query 'Quotas[?QuotaName==\`Number of EIPs - VPC EIPs\`].{Value:Value}' --output text --region $region`

echo -e "Max standard EC2 this region:\t$maxec2\tUsed:\t$usedec2"
echo -e "Max IGW's for this region:\t$maxigw\tUsed:\t$usedigw"
echo -e "Max VPC's for this region:\t$maxvpc\tUsed:\t$usedvpc"
echo -e "Max number of Elastic IP's:\t$maxeip\tUsed:\t$usedeip"

echo "=========================/EC2 max values=============================="
echo ""
echo "======================================================================"